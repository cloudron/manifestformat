'use strict';

const assert = require('assert'),
    CronJob = require('cron').CronJob,
    safe = require('safetydance'),
    semver = require('semver'),
    tv4 = require('tv4').freshApi(),
    validator = require('validator');

const  SCHEMA_V1 = {
    type: 'object',
    properties: {
        'addons': {
            type: 'object',
            patternProperties: {
                '^(ldap|redis|sendmail|oauth|mysql|postgresql|mongodb|localstorage|email|recvmail|tls|turn|docker|proxyAuth)$': {
                    type: 'object',
                    additionalProperties: true
                }
            },
            properties: {
                'oidc': {
                    type: 'object',
                    properties: {
                        loginRedirectUri: {
                            type: 'string'
                        }
                    },
                    additionalProperties: true,
                    required: [ 'loginRedirectUri' ]
                },
                'scheduler': {
                    type: 'object',
                    patternProperties: {
                        '^[a-zA-Z0-9_]+$': {
                            type: 'object',
                            properties: {
                                'schedule': {
                                    type: 'string',
                                    format: 'cronpattern'
                                },
                                'command': {
                                    type: 'string'
                                }
                            }
                        }
                    }
                }
            }
        },
        'aliasableDomain': {
            type: 'boolean'
        },
        'author': {
            type: 'string',
            minLength: 2
        },
        'capabilities': {
            type: 'array',
            items: {
                type: 'string',
                pattern: '^(net_admin|mlock|ping|vaapi)$'
            },
            uniqueItems: true
        },
        'checklist': {
            type: 'object',
            patternProperties: {
                '^[a-zA-Z0-9_\-]+$': {
                    type: 'object',
                    properties: {
                        sso: {
                            type: 'boolean'
                        },
                        message: {
                            type: 'string'
                        }
                    },
                    required: [ 'message' ]
                }
            }
        },
        'changelog': {
            type: 'string',
            minLength: 5
        },
        'configurePath': {
            type: 'string',
            minLength: 1
        },
        'contactEmail': {
            type: 'string',
            format: 'email'
        },
        'description': {
            type: 'string',
            minLength: 5
        },
        'dockerImage': {
            type: 'string',
            minLength: 1
        },
        'documentationUrl': {
            type: 'string',
            format: 'uri',
            minLength: 1
        },
        'forumUrl': {
            type: 'string',
            format: 'uri',
            minLength: 1
        },
        'healthCheckPath': {
            type: 'string',
            minLength: 1
        },
        'httpPort': {
            type: 'integer',
            minimum: 0,
            maximum: 65535
        },
        'httpPorts': {
            type: 'object',
            format: 'envvar',
            patternProperties: {
                '^[a-zA-Z0-9_]+$': {
                    type: 'object',
                    properties: {
                        'title': {
                            type: 'string',
                            minLength: 2
                        },
                        'description': {
                            type: 'string',
                            minLength: 5
                        },
                        'containerPort': {
                            type: 'integer',
                            minimum: 1,
                            maximum: 65535
                        },
                        'defaultValue': {
                            type: 'string',
                            minLength: 1
                        },
                        'aliasableDomain': {
                            type: 'boolean'
                        }
                    },
                    required: [ 'title', 'description', 'containerPort' ]
                }
            }
        },
        'icon': {
            type: 'string'
        },
        'id': {
            type: 'string',
            format: 'reverseDomain'
        },
        'logPaths': {
            type: 'array',
            uniqueItems: true,
            items: {
                type: 'string',
                minLength: 1
            }
        },
        'manifestVersion': {
            type: 'integer',
            minimum: 1,
            maximum: 2
        },
        'maxBoxVersion': {
            type: 'string',
            format: 'semver'
        },
        'mediaLinks': {
            type: 'array',
            uniqueItems: true,
            items: {
                type: 'string',
                format: 'uri'
            }
        },
        'memoryLimit': {
            oneOf: [{
                type: 'integer',
                minimum: 1
            }, {
                type: 'string',
                format: 'byteString'
            }]
        },
        'minBoxVersion': {
            type: 'string',
            format: 'semver'
        },
        'multiDomain': {
            type: 'boolean'
        },
        'optionalSso': {
            type: 'boolean'
        },
        'postInstallMessage': {
            type: 'string',
            minLength: 5
        },
        'runtimeDirs': {
            type: 'array',
            uniqueItems: true,
            items: {
                type: 'string',
                minLength: 1
            }
        },
        'tagline': {
            type: 'string',
            minLength: 5
        },
        'tags': {
            type: 'array',
            items: {
                type: 'string'
            },
            uniqueItems: true
        },
        'targetBoxVersion': {
            type: 'string',
            format: 'semver'
        },
        'tcpPorts': {
            type: 'object',
            format: 'envvar',
            patternProperties: {
                '^[A-Z0-9_]+$': {
                    type: 'object',
                    properties: {
                        'title': {
                            type: 'string',
                            minLength: 2
                        },
                        'description': {
                            type: 'string',
                            minLength: 5
                        },
                        'containerPort': {
                            type: 'integer',
                            minimum: 1,
                            maximum: 65535
                        },
                        'defaultValue': {
                            type: 'integer',
                            minimum: 1,
                            maximum: 65535
                        },
                        'portCount': {
                            type: 'integer',
                            minimum: 1,
                            maximum: 1000
                        },
                        'readOnly': {
                            type: 'boolean'
                        }
                    },
                    required: [ 'title', 'description' ]
                }
            }
        },
        'title': {
            type: 'string',
            minLength: 2
        },
        'udpPorts': {
            type: 'object',
            format: 'envvar',
            patternProperties: {
                '^[A-Z0-9_]+$': {
                    type: 'object',
                    properties: {
                        'title': {
                            type: 'string',
                            minLength: 2
                        },
                        'description': {
                            type: 'string',
                            minLength: 5
                        },
                        'containerPort': {
                            type: 'integer',
                            minimum: 1,
                            maximum: 65535
                        },
                        'defaultValue': {
                            type: 'integer',
                            minimum: 1,
                            maximum: 65535
                        },
                        'portCount': {
                            type: 'integer',
                            minimum: 1,
                            maximum: 1000
                        },
                        'readOnly': {
                            type: 'boolean'
                        }
                    },
                    required: [ 'title', 'description' ]
                }
            }
        },
        'upstreamVersion': {
            type: 'string',
            minLength: 1
        },
        'version': {
            type: 'string',
            format: 'semver'
        },
        'website': {
            type: 'string',
            format: 'uri'
        }
    },
    required: [ 'manifestVersion', 'version', 'httpPort' ]
};

exports = module.exports = {
    isId,
    parse,
    parseString,
    parseFile,
    checkAppstoreRequirements,

    SCHEMA_V1
};

function isId(id) {
    const parts = id.split('.');
    if (parts.length == 1) return false;
    for (const part of parts) {
        if (!/^[a-z0-9][-a-z0-9_]*$/.test(part)) return false;
    }
    return true;
}

// eslint-disable-next-line no-unused-vars
tv4.addFormat('semver', function (data, schema) {
    return semver.valid(data) ? null : 'not a semver';
});

// eslint-disable-next-line no-unused-vars
tv4.addFormat('envvar', function (data, schema) {
    for (const env of Object.keys(data)) {
        if (env.startsWith('CLOUDRON_')) return 'CLOUDRON_ prefix is reserved';
    }
    return null;
});

// eslint-disable-next-line no-unused-vars
tv4.addFormat('uri', function (data, schema) {
    var options = {
        protocols: [ 'http', 'https' ],
        require_tld: true,
        require_protocol: false,
        allow_underscores: false,
        host_whitelist: false,
        host_blacklist: false
    };

    if (!validator.isURL(data, options)) return 'Invalid URL';

    return null;
});

// eslint-disable-next-line no-unused-vars
tv4.addFormat('reverseDomain', function (data, schema) {
    return isId(data) ? null : 'Invalid id';
});

// eslint-disable-next-line no-unused-vars
tv4.addFormat('email', function (data, schema) {
    return validator.isEmail(data) ? null : 'Invalid email';
});

// eslint-disable-next-line no-unused-vars
tv4.addFormat('cronpattern', function (data, schema) {
    const NAMED_PATTERNS = [ '@service', '@reboot', '@yearly', '@annually', '@monthly', '@weekly', '@daily', '@hourly' ];
    if (NAMED_PATTERNS.includes(data)) return null;

    if (data.split(' ').length !== 5) return 'Invalid cron pattern (note: seconds disallowed)'; // second is disallowed

    try {
        new CronJob('00 ' + data, function() { // second is disallowed
        });
        return null;
    } catch(ex) {
        return `Invalid cron pattern: ${ex.message}`;
    }
});

// eslint-disable-next-line no-unused-vars
tv4.addFormat('byteString', function (data, schema) {
    const  results = data.match(/^((-|\+)?(\d+(?:\.\d+)?)) *(kb|mb|gb|tb)$/i);
    return !results ? 'Invalid size' : null;
});

function parse(manifest) {
    assert(manifest && typeof manifest === 'object');

    const result = tv4.validateResult(manifest, SCHEMA_V1, false /* recursive */, true /* banUnknownProperties */);
    if (!result.valid) return new Error(result.error.message + (result.error.dataPath ? ' @ ' + result.error.dataPath : ''));

    let envVars = [];
    if ('tcpPorts' in manifest) envVars = envVars.concat(Object.keys(manifest.tcpPorts));
    if ('udpPorts' in manifest) envVars = envVars.concat(Object.keys(manifest.udpPorts));
    if ('httpPorts' in manifest) envVars = envVars.concat(Object.keys(manifest.httpPorts));
    if ([...new Set(envVars)].length !== envVars.length) return new Error('Duplicate env vars in tcpPorts/udpPorts/httpPorts');

    // https://docs.docker.com/engine/reference/commandline/tag/#extended-description
    if ('dockerImage' in manifest) {
        if (manifest.dockerImage.includes('//')) return new Error('Invalid docker image name'); // seems to be a common mistake to paste 'https://'
    }

    if ('runtimeDirs' in manifest) {
        const ALLOWED_DIRS = [ '/root', '/app/code', '/home/cloudron' ];
        for (const dir of manifest.runtimeDirs) {
            if (ALLOWED_DIRS.every(ad => dir !== ad && !dir.startsWith(ad))) return new Error('Invalid run time dir');
        }
    }

    const addons = manifest.addons;
    if (addons?.localstorage?.sqlite) {
        const sqlite = addons.localstorage.sqlite;
        if (!sqlite || typeof sqlite !== 'object') return new Error('Expecting sqlite to be an object');
        if (!sqlite.paths) return new Error('sqlite.paths is required');
        if (!Array.isArray(sqlite.paths) || sqlite.paths.some(p => typeof p !== 'string')) return new Error('Expecting sqlite.paths to be an array of strings');
    }

    return null;
}

function parseString(manifestJson) {
    assert(typeof manifestJson === 'string');

    const  manifest = safe.JSON.parse(manifestJson);

    const  error = manifest ? parse(manifest) : new Error('Unable to parse manifest: ' + safe.error.message);

    return { manifest: error ? null : manifest, error: error };
}

function parseFile(manifestFile) {
    assert(typeof manifestFile === 'string');

    const  manifestJson = safe.fs.readFileSync(manifestFile, 'utf8');
    if (!manifestJson) return { manifest: null, error: new Error('Unable to read file: ' + safe.error.message) };

    return parseString(manifestJson);
}

// tags which are required by appstore (but not required for installation)
function checkAppstoreRequirements(manifest) {
    assert(typeof manifest === 'object');

    if (!('dockerImage' in manifest)) return new Error('dockerImage is missing in manifest');
    if (!manifest.dockerImage.startsWith(`cloudron/${manifest.id}:`)) return new Error(`dockerImage must be in cloudron/appid format (${manifest.dockerImage})`);

    const requiredFields = [ 'id', 'title', 'description', 'tagline', 'website', 'contactEmail', 'author', 'tags', 'icon', 'changelog' ];
    for (const field of requiredFields) {
        if (!(field in manifest)) return new Error(`${field} is missing in manifest`);
        if (!manifest[field]) return new Error(`${field} is empty in manifest`);
    }

    if (!('mediaLinks' in manifest)) return new Error('mediaLinks is missing in manifest');
    if (manifest.mediaLinks.length === 0) return new Error('mediaLinks is empty in manifest');

    if (manifest.addons && Object.keys(manifest.addons).some(function (addon) { return /^_.*$/.test(addon); })) {
        return new Error('cannot publish with reserved addons');
    }

    if (!('healthCheckPath' in manifest)) return new Error('healthCheckPath is missing in manifest');

    const p = semver.parse(manifest.version);
    if (p.prerelease.length === 1 && typeof p.prerelease[0] !== 'number') return new Error('Version can only contain a numeric prerelease');
    if (p.build.length !== 0) return new Error('Build cannot contains build');

    if (manifest.upstreamVersion && (!manifest.minBoxVersion || semver.lt(manifest.minBoxVersion, '7.1.0'))) return new Error('upstreamVersion requires minBoxVersion of atleast 7.1.0');
    if (manifest.logPaths && (!manifest.minBoxVersion || semver.lt(manifest.minBoxVersion, '7.1.0'))) return new Error('logPaths requires minBoxVersion of atleast 7.1.0');
    if (manifest.runtimeDirs && (!manifest.minBoxVersion || semver.lt(manifest.minBoxVersion, '7.3.3'))) return new Error('runtimeDirs requires minBoxVersion of atleast 7.3.3');

    for (const n in manifest.tcpPorts) {
        if (manifest.tcpPorts[n].portCount && (!manifest.minBoxVersion || semver.lt(manifest.minBoxVersion, '7.7.0'))) return new Error('tcpPorts.portCount requires minBoxVersion of atleast 7.7.0');
    }
    for (const n in manifest.udpPorts) {
        if (manifest.udpPorts[n].portCount && (!manifest.minBoxVersion || semver.lt(manifest.minBoxVersion, '7.7.0'))) return new Error('udpPorts.portCount requires minBoxVersion of atleast 7.7.0');
    }

    if (manifest.checklist && (!manifest.minBoxVersion || semver.lt(manifest.minBoxVersion, '8.0.0'))) return new Error('checklist requires minBoxVersion of atleast 8.0.0');

    if (manifest.addons?.localstorage?.sqlite && (!manifest.minBoxVersion || semver.lt(manifest.minBoxVersion, '8.2.0'))) return new Error('sqlite addon requires minBoxVersion of atleast 8.2.0');

    if (manifest.addons?.sendmail?.requiresValidCertificate && (!manifest.minBoxVersion || semver.lt(manifest.minBoxVersion, '9.0.0'))) return new Error('requiresValidCertificate requires minBoxVersion of atleast 9.0.0');

    return null;
}
