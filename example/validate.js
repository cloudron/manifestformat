'use strict';

const manifestfile = 'CloudronManifest.json';
const manifestModule = 'cloudron-manifestformat';

try {
    require.resolve(manifestModule);
} catch (_) {
    console.error('module ´' + manifestModule + '´ not found.');
    process.exit(1);
}

const manifestformat = require(manifestModule);
let result = manifestformat.parseFile(manifestfile);

if (result.error === null) {
    if (process.argv.length === 3 && process.argv[2] === '--strict') {
        console.info('Strict validation...');
        result = manifestformat.checkAppstoreRequirements(result.manifest);
        if (result !== null) {
            console.error(result.message);
            process.exit(2);
        }
    }

    console.info(manifestfile + ' validated successfully.');
} else {
    console.error(result.error.message);
    process.exit(1);
}
