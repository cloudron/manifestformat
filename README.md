Cloudron Manifest Validation
============================

This package validates the Cloudron Manifest

## Build
```bash
npm test
```

## Install

```bash
npm install --global cloudron-manifestformat
```

## Sample Usage
* see folder `example`
* validates `CloudronManifest.json`
* e.g. for use within git pre-commmit hook
* copy `validate.js` to your Cloudron-App root dir
* run validations

Run basic validation
```bash
npm validate.js
```

Extended validation for use on marketplace:
```bash
npm validate.js --strict
```
